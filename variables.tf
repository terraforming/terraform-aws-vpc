variable "deployment_id" {
  description = "Module deployment ID (may be generated with terraform-deployment-id module)"
}

variable "name" {
  description = "Name tag for VPC"
  default     = "Main"
}

variable "cidr" {
  description = "CIDR block for VPC"
  default     = "10.8.0.0/16"
}

variable "public_subnets_cidr" {
  description = "List of CIDR blocks for Public Subnets"
  type        = "list"
  default     = ["10.8.16.0/20", "10.8.48.0/20", "10.8.80.0/20", "10.8.112.0/20", "10.8.144.0/20", "10.8.176.0/20"]
}
