resource "aws_vpc" "main" {
  cidr_block           = "${var.cidr}"
  enable_dns_hostnames = "true"

  tags {
    Name          = "${var.name}"
    deployment_id = "${var.deployment_id}"
  }
}

data "aws_availability_zones" "all" {}

resource "aws_subnet" "public" {
  count             = "${length(data.aws_availability_zones.all.names)}"
  vpc_id            = "${aws_vpc.main.id}"
  cidr_block        = "${element(var.public_subnets_cidr, count.index)}"
  availability_zone = "${element(data.aws_availability_zones.all.names, count.index)}"

  tags {
    Name          = "Public"
    deployment_id = "${var.deployment_id}"
  }
}

resource "aws_default_route_table" "main" {
  default_route_table_id = "${aws_vpc.main.default_route_table_id}"

  tags {
    Name          = "${var.name}"
    deployment_id = "${var.deployment_id}"
  }
}

resource "aws_internet_gateway" "main" {
  vpc_id = "${aws_vpc.main.id}"

  tags {
    Name          = "${var.name}"
    deployment_id = "${var.deployment_id}"
  }
}

resource "aws_route" "default" {
  route_table_id         = "${aws_default_route_table.main.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.main.id}"
}

resource "aws_route_table_association" "default" {
  count          = "${length(data.aws_availability_zones.all.names)}"
  subnet_id      = "${element(aws_subnet.public.*.id, count.index)}"
  route_table_id = "${aws_default_route_table.main.id}"
}
