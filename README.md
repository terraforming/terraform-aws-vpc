# AWS VPC Terraform module

VPC with Public subnet is each AZ and basic routing for internet access.

## Usage

    module "vpc" {
        source        = "git::https://bitbucket.org/terraforming/terraform-aws-vpc.git?ref=master"
        deployment_id = "SOMETHING_UNIQUE"
    }

## Outputs

| Name | Description |
|------|-------------|
| id | VPC ID |
| public_subnets | List of Public Subnets IDs |

## Variables

| Name | Default | Description |
|------|---------|-------------|
| deployment_id | | Module deployment ID (may be generated with [terraform-deployment-id](https://bitbucket.org/terraforming/terraform-deployment-id) module) |
| name | Main | Name tag for VPC |
| cidr | 10.8.0.0/16 | CIDR block for VPC |
| public_subnets_cidr | ["10.8.16.0/20", "10.8.48.0/20", "10.8.80.0/20", "10.8.112.0/20", "10.8.144.0/20", "10.8.176.0/20"] | List of CIDR blocks for Public Subnets |
