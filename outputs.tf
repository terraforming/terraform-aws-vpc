output "id" {
  description = "VPC ID"
  value       = "${aws_vpc.main.id}"
}

output "public_subnets" {
  description = "List of Public Subnets IDs"
  value       = "${aws_subnet.public.*.id}"
}
